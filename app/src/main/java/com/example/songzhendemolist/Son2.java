package com.example.songzhendemolist;

import android.util.Log;

/**
 * The author：dp
 * Date：2022/3/2 10:20
 * Des:
 */
public abstract class Son2  extends Father{

    private void test(){

    }

    @Override
    protected void doSomeThing() {
        super.doSomeThing();
        Log.d("sz--","Son2 doSomeThing----");
    }

    @Override
    protected void doSomeThing(String str) {
        super.doSomeThing(str);
    }


}
