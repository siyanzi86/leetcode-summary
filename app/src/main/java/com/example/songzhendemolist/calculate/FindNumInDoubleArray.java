package com.example.songzhendemolist.calculate;

import android.util.Log;

/**
 * The author：dp
 * Date：2022/8/4 10:12
 * Des:在一个二维数组中，每一行都按照从左到右递增的顺序排序，每一列都按照从上到 下递增的顺序排序。请完成一个函数，输入这样的一个二维数组和一个整数，判断 数组中是否含有该整数
 * <p>
 * 解题思路 首先选取数组中右上角的数字。如果该数字等于要查找的数字，查找过程结束。
 * 如果该数字大于要查找的数字，剔除这个数字所在的列：如果该数字小于要查找的 数字，剔除这个数字所在的行。
 * 也就是说如果要查找的数字不在数组的右上角，则每－次都在数组的查找范围中剔 除行或者一列，这样每一步都可以缩小查找的范围，直到找到要查找的数字，或者 查找范围为空。
 */
public class FindNumInDoubleArray implements ICalculate {


    @Override
    public void calculate() {

        int[][] arr = {{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}};

        Log.d("sz--",find(arr,11)+"----");

    }

    public boolean find(int[][] arr, int num) {
        if (arr == null || arr.length == 0 || arr[0].length == 0) {
            return false;
        }
        int rows = arr.length;
        int cols = arr[0].length;

        int startRow = 0;
        int startCol = cols - 1;

        while (startRow <= rows && startCol >= 0) {
            int point = arr[startRow][startCol];
            if (point == num) {
                return true;
            } else if (point > num) {
                startCol--;
            } else {
                startRow++;
            }
        }

        return false;
    }
}
