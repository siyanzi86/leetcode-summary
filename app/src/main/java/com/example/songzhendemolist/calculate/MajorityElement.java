package com.example.songzhendemolist.calculate;

import java.util.HashMap;
import java.util.Map;

/**
 * The author：dp
 * Date：2022/9/1 10:17
 * Des:数组中有一个数字出现的次数超过数组长度的一半，请找出这个数字。   思路：)长度大于一半说明大于其他出现次数的总和   目标值和不一样的值可以相互抵消  剩下的肯定是出现最多的
 * 你可以假设数组是非空的，并且给定的数组总是存在多数元素。
 */
public class MajorityElement implements ICalculate {
    @Override
    public void calculate() {

    }

    /**
     * topK 算法
     * @param arr
     * @return
     */
    public int halfElement(int[] arr) {
        HashMap<Integer, Integer> counts = new HashMap<>();

        for (int num : arr) {
            if (!counts.containsKey(num)) {
                counts.put(num, 1);
            } else {
                counts.put(num, counts.get(num) + 1);
            }
        }

        Map.Entry<Integer, Integer> maxEntry = null;
        for (Map.Entry<Integer, Integer> entry
                : counts.entrySet()
        ) {
            if (maxEntry == null || maxEntry.getValue() < entry.getValue()) {
                maxEntry = entry;
            }
        }
        return maxEntry.getKey();
    }


    /**
     * 投票算法   只有票数为0时 重置候选数字
     * @return
     */
    public int majorityElement(int[] nums) {
        if (nums == null || nums.length < 1) {
            throw new IllegalArgumentException("入参错误");
        }

        int result = nums[0];

        int count = 1;

        for (int i = 1; i < nums.length; i++) {

            if (count == 0) {
                result = nums[i];
                count = 1;
            } else if (result == nums[i]) {
                count++;
            } else {
                count--;
            }
        }

        count = 0;

        for (int val : nums) {
            if (val == result) {
                count++;
            }
        }

        if (count > nums.length >>> 1) {
            return result;
        } else {
            throw new IllegalArgumentException("invalid input");
        }
    }


}
