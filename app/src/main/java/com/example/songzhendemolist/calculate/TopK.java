package com.example.songzhendemolist.calculate;


import android.util.Log;

import java.util.Arrays;

/**
 * The author：dp
 * Date：2022/9/8 20:41
 * Des:足够长的数组   元素全为0   遍历拿到一个数作为数组下标给元素值加1
 */
public class TopK implements ICalculate {
    @Override
    public void calculate() {
        int[] arr = new int[]{1, 1, 2, 2, 5, 5, 5, 6, 7, 8, 9, 9};
        getTopK(arr);
    }

    private int[] getTopK(int[] arr) {
        int[] a = new int[10];
        int[] b = new int[10];
        int[] ans = new int[arr.length];


        for (int i = 0; i < arr.length; i++) {
            a[arr[i]] = a[arr[i]] + 1;
        }

//        a {0,2,2,0,0,3,1,1,1,2}


        for (int i = 0, j = 0; i < a.length; i++) {
            if (a[i] != 0) {
                b[j++] = a[i];
            }
        }
        // b {2,2,3,1,1,1,2,0,0,0}

        Arrays.sort(b);
        // b {0,0,0,1,1,1,2,2,2,3}
        int index = 0;

        for (int i = 0; i < b.length; i++) {
            if (b[i] != 0) {
                for (int j = 0; j < a.length; j++) {
                    if (a[j] != 0) {
                        if (b[i] == a[j]) {
                            a[j] = 0;
                            for (int k = 0; k < b[i]; k++) {
                                ans[index++] = j;
                            }
                            break;
                        }
                    }
                }
            }
        }

        return ans;
    }
}
