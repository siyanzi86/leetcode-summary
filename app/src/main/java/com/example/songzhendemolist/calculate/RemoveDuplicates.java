package com.example.songzhendemolist.calculate;


/**
 * https://leetcode.cn/problems/remove-duplicates-from-sorted-array/description/
 */
public class RemoveDuplicates implements ICalculate {
    @Override
    public void calculate() {

    }

    public int removeDuplicates(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }

        int slow = 1;
        int fast = 1;

        while (fast < nums.length) {

            if (nums[fast] != nums[fast - 1]) {
                nums[slow] = nums[fast];
                slow++;
            }
            fast++;
        }


        return slow;
    }
}
