package com.example.songzhendemolist.calculate;

import android.util.Log;

import java.util.HashSet;
import java.util.Set;

/**
 * The author：dp
 * Date：2022/3/2 15:44
 * Des:无重复字符的最长子串
 * 示例 1:
 * <p>
 * 输入: s = "abcabcbb"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
 * 示例 2:
 * <p>
 * 输入: s = "bbbbb"
 * 输出: 1
 * 解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/longest-substring-without-repeating-characters
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class LengthOfLongestSubstring implements ICalculate {

    @Override
    public void calculate() {
        Log.d("sz--", "-------------->" + lengthOfLongestSubstring("pwwkew"));
    }

    public int lengthOfLongestSubstring(String s) {
        if (s.length() == 0) {
            return 0;
        }
        Set<Character> set = new HashSet<>();

        int index = 0;
        int max = 0;

        for (int i = 0; i < s.length(); i++) {

            if (index != 0) {
                set.remove(s.charAt(i - 1));
            }

            while (index < s.length() && !set.contains(s.charAt(index))) {
                set.add(s.charAt(index));
                index++;
            }

            max = Math.max(max, index - i);
        }

        return max;
    }
}
