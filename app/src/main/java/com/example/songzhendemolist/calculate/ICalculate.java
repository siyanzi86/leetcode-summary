package com.example.songzhendemolist.calculate;

/**
 * The author：dp
 * Date：2022/2/28 17:00
 * Des:
 */
public interface ICalculate {

    void calculate();
}
