package com.example.songzhendemolist.calculate;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * The author：dp
 * Date：2022/3/23 14:38
 * Des:请你来实现一个 myAtoi(string s) 函数，使其能将字符串转换成一个 32 位有符号整数（类似 C/C++ 中的 atoi 函数）。 思路：1.去掉前导空格  2.取符号 3.逐个遍历判断是否为数字 {
 * 是：已经取出的数字*10+当前数字 否：继续遍历  判断是否是int{
 * 是 继续添加  否跳出循环
 * }
 * }
 * <p>
 * 判断是否为数字  字符阿斯克码 ‘0’<'9'  48-57
 * <p>
 * 状态机
 * https://leetcode.cn/problems/string-to-integer-atoi/
 */
public class MyAtoi implements ICalculate {

    @Override
    public void calculate() {
        Log.d("sz--", myAtoi("-12345mm") + "------");
    }

    public int myAtoi(String s) {
        AutoMato autoMato = new AutoMato();
        for (int i = 0; i < s.length(); i++) {
            autoMato.get(s.charAt(i));
        }
        return (int) (autoMato.sign * autoMato.ans);
    }

    public static class AutoMato {
        long ans = 0;
        int sign = 1;
        String status = "start";
        Map<String, String[]> table = new HashMap<String, String[]>() {{
            put("start", new String[]{"start", "sign", "num", "end"});
            put("sign", new String[]{"end", "end", "num", "end"});
            put("num", new String[]{"end", "end", "num", "end"});
            put("end", new String[]{"end", "end", "end", "end"});
        }};

        public void get(char c) {
            status = table.get(status)[getStatus(c)];
            if (status.equals("num")) {
                ans = ans * 10 + c - '0';//从零到目标数字是多少
                ans = sign == 1 ? Math.min(ans, (long) Integer.MAX_VALUE) : Math.min(ans, -(long) Integer.MIN_VALUE);
            } else if (status.equals("sign")) {
                sign = c == '-' ? -1 : 1;
            }
        }


        public int getStatus(char c) {
            if (c == ' ') {
                return 0;
            } else if (c == '+' || c == '-') {
                return 1;
            } else if (Character.isDigit(c)) {
                return 2;
            }
            return 3;
        }
    }


}
