package com.example.songzhendemolist.calculate;

/**
 * The author：dp
 * Date：2022/9/14 09:49
 * Des: 长度最小的子数组
 * 给定一个含有 n 个正整数的数组和一个正整数 target 。
 * <p>
 * 找出该数组中满足其和 ≥ target 的长度最小的 连续子数组 [numsl, numsl+1, ..., numsr-1, numsr] ，并返回其长度。如果不存在符合条件的子数组，返回 0 。
 * https://leetcode.cn/problems/minimum-size-subarray-sum/
 */
public class MinSubArrayLen implements ICalculate {
    @Override
    public void calculate() {

    }

    public int minSubArrayLen(int target, int[] nums) {
        int len = nums.length;

        if (len == 0) return 0;

        int min = Integer.MAX_VALUE;

        int start = 0, end = 0;

        int sum = 0;


        while (end < len) {
            sum += nums[end];

            while (sum >= target) {
                min = Math.min(min, end - start + 1);
                sum -= nums[start];
                start++;
            }
            end++;
        }
        return min == Integer.MAX_VALUE ? 0 : min;
    }
}
