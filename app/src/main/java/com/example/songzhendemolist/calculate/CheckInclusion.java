package com.example.songzhendemolist.calculate;

import java.util.Arrays;

/**
 * The author：dp
 * Date：2022/9/26 10:07
 * Des:给你两个字符串 s1 和 s2 ，写一个函数来判断 s2 是否包含 s1 的排列。如果是，返回 true ；否则，返回 false 。
 * <p>
 * 换句话说，s1 的排列之一是 s2 的 子串
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode.cn/problems/permutation-in-string
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class CheckInclusion implements ICalculate {
    @Override
    public void calculate() {

    }

    public boolean checkInclusion(String s1, String s2) {
        int n = s1.length();
        int m = s2.length();
        if (n>m) {
            return false;
        }

        int[] cs1 = new int[26];
        int[] cs2 = new int[26];


        for (int i = 0; i < n; i++) {
            ++cs1[s1.charAt(i) - 'a'];
            ++cs2[s2.charAt(i) - 'a'];
        }
        if (Arrays.equals(cs1, cs2)) {
            return true;
        }

        for (int i = n; i < m; i++) {
            ++cs2[s2.charAt(i) - 'a'];
            --cs2[s2.charAt(i - n) - 'a'];
            if (Arrays.equals(cs1, cs2)) {
                return true;
            }
        }

        return false;
    }
}