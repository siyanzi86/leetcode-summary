package com.example.songzhendemolist.calculate;

/**
 * The author：dp
 * Date：2022/12/30 10:34
 * Des:
 * 给定一个由 整数 组成的 非空 数组所表示的非负整数，在该数的基础上加一。
 * <p>
 * 最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。
 * <p>
 * 你可以假设除了整数 0 之外，这个整数不会以零开头。  1  2  9
 */
public class PlusOne {

    public int[] plusOne(int[] digits) {

        int len = digits.length - 1;

        for (int i = len; i >= 0; i--) {
            if (digits[i] == 9) {
                digits[i] = 0;
            } else {
              digits[i]++;
              return digits;
            }
        }

        int[] arr = new int[digits.length + 1];
        arr[0] = 1;
        return arr;

    }
}
