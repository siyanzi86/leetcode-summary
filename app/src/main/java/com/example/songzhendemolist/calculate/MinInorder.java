package com.example.songzhendemolist.calculate;

import android.util.Log;

/**
 * The author：dp
 * Date：2022/8/12 11:00
 * Des:把一个数组最开始的若干个元素搬到数组的末尾，
 * 我们称之数组的旋转。输入一个 递增排序的数组的一个旋转，
 * 输出旋转数组的最小元素。例如数组{3,4,5,1,2 ｝为 ｛ 1,2,3,4,5}的一个旋转，
 * 该数组的最小值为1。
 * 二、解题思路
 * Step1.和二分查找法一样，我们用两个指针分别指向数组的第一个元素和最后一个 元素。
 * Step2.接着我们可以找到数组中间的元素： 如果该中间元素位于前面的递增子数组，那么它应该大于或者等于第一个指针指向 的元素。
 * 此时数组中最小的元素应该位于该中间元素的后面。我们可以把第一个指 针指向该中间元素，这样可以缩小寻找的范围。如果中间元素位于后面的递增子数 组
 * ，那么它应该小于或者等于第二个指针指向的元素。此时该数组中最小的元素应 该位于该中间元素的前面。
 * Step3.接下来我们再用更新之后的两个指针，重复做新一轮的查找。
 */
public class MinInorder implements ICalculate {
    @Override
    public void calculate() {
        int[] arr = {13, 14, 15, 2, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        Log.d("sz--", "min  " + minInorder(arr));
    }

    private int minInorder(int[] numbers) {
        if (numbers == null || numbers.length == 0) {
            throw new RuntimeException("Invalid input");
        }

        int start = 0;
        int end = numbers.length - 1;
        while (start < end) {
            int min = (start + end) / 2;

            if (numbers[min] > numbers[end]) {
                start = min + 1;
            } else if (numbers[min] < numbers[end]) {
                end = min;
            } else {
                end -= 1;
            }

        }
        return numbers[start];
    }
}
