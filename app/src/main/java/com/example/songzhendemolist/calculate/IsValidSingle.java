package com.example.songzhendemolist.calculate;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class IsValidSingle implements ICalculate {
    @Override
    public void calculate() {
    }

    public boolean isValid(String s) {
        int length = s.length();

        if ((length & 1) == 1) {
            return false;
        }

        Map<Character, Character> pair = new HashMap<Character, Character>() {{
            put(')', '(');
            put('}', '{');
            put(']', '[');
        }};

        Deque<Character> deque = new LinkedList<>();

        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);

            if (pair.containsKey(ch)) {
                if (deque.isEmpty() || pair.get(ch) != deque.peek()) {
                    return false;
                } else {
                    deque.pop();
                }

            } else {
                deque.push(ch);
            }
        }

        return deque.isEmpty();
    }
}
