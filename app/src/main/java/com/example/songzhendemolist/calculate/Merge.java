package com.example.songzhendemolist.calculate;

/**
 * The author：dp
 * Date：2023/1/4 10:04
 * Des:
 * <p>
 * 输入：nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
 * 输出：[1,2,2,3,5,6]
 * 解释：需要合并 [1,2,3] 和 [2,5,6] 。
 * 合并结果是 [1,2,2,3,5,6] ，其中斜体加粗标注的为 nums1 中的元素。
 * https://leetcode.cn/problems/merge-sorted-array/
 */
public class Merge implements ICalculate {
    @Override
    public void calculate() {

    }


    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int p0 = 0;

        int p1 = 0;

        int[] ansArr = new int[m + n];

        while (p0 < m || p1 < n) {
            int current = 0;
            if (p0 == m) {
                current = nums2[p1++];
            } else if (p1 == n) {
                current = nums1[p0++];
            } else if (nums1[p0] < nums2[p1]) {
                current = nums1[p0++];
            } else {
                current = nums2[p1++];
            }

            ansArr[p0 + p1 - 1] = current;
        }

        for (int i = 0; i < m + n; i++) {
            nums1[i] = ansArr[i];
        }
    }
}
