package com.example.songzhendemolist.calculate;

/**
 * The author：dp
 * Date：2022/12/23 09:54
 * Des:移除元素
 * https://leetcode.cn/problems/remove-element/
 */
public class RemoveElement implements ICalculate {
    @Override
    public void calculate() {
        removeElement(new int[]{3,2,2,3},3);
    }

    /**
     * 输入：nums = [3,2,2,3], val = 3
     * 输出：2, nums = [2,2]
     */
    public int removeElement(int[] nums, int val) {
        if (nums.length == 0) {
            return 0;
        }
        int left = 0, right = nums.length;

        while (left < right) {
            if (nums[left] == val) {
                nums[left] = nums[right-1];
                right--;
            } else {
                left++;
            }
        }

        return left;
    }
}
