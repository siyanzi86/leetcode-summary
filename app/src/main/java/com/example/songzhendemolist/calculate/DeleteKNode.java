package com.example.songzhendemolist.calculate;

import java.util.Deque;
import java.util.LinkedList;

/**
 * The author：dp
 * Date：2022/8/19 10:13
 * Des:删除K节点
 */
public class DeleteKNode implements ICalculate {
    @Override
    public void calculate() {
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        deleteN(node1, 2);

    }

    //删除节点和原题不一样
    public ListNode deleteNode(ListNode head, int val) {
        if (head.val == val) {
            return head.next;
        }

        ListNode pre = head;
        ListNode current = head.next;

        while (current != null && current.val != val) {
            pre = current;
            current = current.next;
        }
        if (current != null) {
            pre.next = current.next;
        }

        return head;
    }

    public ListNode deleteN(ListNode head, int n) {
        ListNode dummy = new ListNode(0, head);
        Deque<ListNode> stack = new LinkedList<ListNode>();
        ListNode cur = dummy;
        while (cur != null) {
            stack.push(cur);
            cur = cur.next;
        }
        for (int i = 0; i < n; ++i) {
            stack.pop();
        }
        ListNode prev = stack.peek();
        prev.next = prev.next.next;
        ListNode ans = dummy.next;
        return ans;
    }


    public ListNode deleteK(ListNode head, int k) {


        ListNode dummy = new ListNode(0, head);

        ListNode first = head;
        ListNode second = dummy;
        for (int i = 0; i < k; i++) {
            if (first != null) {
                first = first.next;
            }
        }

        while (first != null) {
            first = first.next;
            second = second.next;
        }

        second.next = second.next.next;

        return dummy.next;

    }
}
