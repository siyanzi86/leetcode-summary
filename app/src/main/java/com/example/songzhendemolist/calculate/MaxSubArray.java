package com.example.songzhendemolist.calculate;

/**
 * The author：dp
 * Date：2022/9/5 10:09
 * Des: 连续数组的子数组的最大和
 */
public class MaxSubArray implements ICalculate {
    @Override
    public void calculate() {

    }

    public int maxSubArray(int[] nums) {
        if (nums == null || nums.length < 1) {
            return 0;
        }
        int max = Integer.MIN_VALUE;
        int current = 0;

        for (int num : nums) {
            if (current <= 0) {
                current = num;
            }else {
                current = current + num;
            }

            max = Math.max(max, current);
        }

        return max;
    }
}
