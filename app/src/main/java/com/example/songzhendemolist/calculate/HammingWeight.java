package com.example.songzhendemolist.calculate;

import android.util.Log;

/**
 * 二进制中1的个数
 * 编写一个函数，输入是一个无符号整数（以二进制串的形式），返回其二进制表达式中数字位数为 '1' 的个数（也被称为 汉明重量).）。
 */

public class HammingWeight implements ICalculate {
    @Override
    public void calculate() {
        Log.d("sz--", "result--->" + hammingWeight(00000000000000000000000000000001));
        for (int i = 0; i < 10; i++) {
            Log.d("sz--", Integer.toBinaryString(1 << i));
        }

        Log.d("sz--", (1 & 0) + "   wwww");
    }

    public int hammingWeight(int n) {//将n与1、 10、 100、1000....进行与运算  同为1 结果为1  其他为0
        int ret = 0;
        for (int i = 0; i < 32; i++) {
            if ((n & (1 << i)) != 0) {
                ret++;
            }
        }
        return ret;
    }

    public int hammingWeight1(int n) {//将n的最低位的1变成0  直到n变成0
        int result = 0;

        while (n != 0) {
            result++;
            n = n & (n - 1);
        }

        return result;
    }
}
