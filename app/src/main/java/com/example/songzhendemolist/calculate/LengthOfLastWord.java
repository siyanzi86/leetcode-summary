package com.example.songzhendemolist.calculate;

/**
 * The author：dp
 * Date：2022/12/30 10:00
 * Des:
 */
public class LengthOfLastWord {

    /**
     * 给你一个字符串 s，由若干单词组成，单词前后用一些空格字符隔开。返回字符串中 最后一个 单词的长度。
     * <p>
     * 单词 是指仅由字母组成、不包含任何空格字符的最大子字符串。
     * https://leetcode.cn/problems/length-of-last-word/
     *
     * @param s
     * @return
     */
    public int lengthOfLastWord(String s) {
        int index = s.length() - 1;
        int ans = 0;

        while (index >= 0 && s.charAt(index) == ' ') {
            index--;
        }

        while (index >= 0 && s.charAt(index) != ' ') {
            index--;
            ans++;
        }

        return ans;
    }
}
