package com.example.songzhendemolist.calculate;

/**
 * The author：dp
 * Date：2022/8/23 10:13
 * Des: 合并递增链表 合并完成依然是递增的
 */
public class MergeNode implements ICalculate {
    @Override
    public void calculate() {

    }

    public ListNode mergeTwoListsByDiGui(ListNode l1, ListNode l2) {

        if (l1 == null){
            return l2;
        }
        if (l2 == null){
            return l1;
        }

        if (l1.val<l2.val){
            l1.next = mergeTwoListsByDiGui(l1.next,l2);
            return l1;
        }else {
            l2.next = mergeTwoListsByDiGui(l1,l2.next);
            return l2;
        }
    }


    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        }
        if (l2 == null) {
            return l1;
        }
        ListNode dummy = new ListNode();
        ListNode pointer = dummy;
        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                pointer.next = l1;
                l1 = l1.next;
            } else {
                pointer.next = l2;
                l2 = l2.next;
            }

            pointer = pointer.next;
        }

        pointer.next = l1 != null ? l1 : l2;

        return dummy.next;

    }
}
