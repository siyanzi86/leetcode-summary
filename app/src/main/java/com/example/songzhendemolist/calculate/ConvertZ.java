package com.example.songzhendemolist.calculate;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * The author：dp
 * Date：2022/3/9 10:13
 * Des:
 */
public class ConvertZ implements ICalculate {

    @Override
    public void calculate() {
        Log.d("sz--", "--------------->" + convert2("PAYPALISHIRING", 2));

        for (int i = 0; i < 10; i++) {
            Log.d("sz--", i + "------" + (i + 1) % 3);
        }
    }

    /**
     * 矩阵
     */
    public String convertZ(String s, int numRows) {
        if (numRows < 2 || numRows >= s.length()) {
            return s;
        }

        int t = numRows + numRows - 2;
        int colInt = numRows - 1;
        int tNum = (s.length() + t - 1) / t;
        int rows = numRows;
        int cols = tNum * colInt;

        char[][] chars = new char[rows][cols];

        for (int i = 0, x = 0, y = 0; i < s.length(); i++) {
            chars[x][y] = s.charAt(i);
            if (i % t < numRows - 1) {
                ++x;//向下
            } else {
                --x; //向上
                ++y;//斜向上向右
            }
        }
        StringBuilder sb = new StringBuilder();

        for (char[] cs : chars) {
            for (char e : cs) {
                if (e != 0) {
                    sb.append(e);
                }
            }
        }

        return sb.toString();

    }

    /**
     * 标识位反转
     *
     * @param s
     * @param numRows
     * @return
     */
    public String convert(String s, int numRows) {
        if (s == null || s.length() <= numRows) {
            return s;
        }
        StringBuilder[] stringBuilders = new StringBuilder[numRows];

        for (int i = 0; i < numRows; i++) {
            stringBuilders[i] = new StringBuilder();
        }

        int key = -1;
        int i = 0;
        for (Character c : s.toCharArray()) {
            stringBuilders[i].append(c);
            if (i == 0 || i == numRows - 1) {
                key = -key;
            }
            i += key;
        }
        StringBuilder ans = new StringBuilder();
        for (StringBuilder sb : stringBuilders) {
            ans.append(sb);
        }

        return ans.toString();
    }


    public String convert3(String s, int r) {

        if (r < 2 || r >= s.length()) {
            return s;
        }
        int key = -1;//决定方向
        StringBuilder[] stringBuilders = new StringBuilder[r];
        for (int i = 0; i < r; i++) {
            stringBuilders[i] = new StringBuilder();
        }
        int i = 0;//从左到右  从上到下
        for (char c : s.toCharArray()) {
            stringBuilders[i].append(c);
            if (i == 0 || i == r - 1) {
                key = -key;
            }
            i += key;
        }
        StringBuilder result = new StringBuilder();
        for (StringBuilder b :
                stringBuilders) {
            result.append(b);
        }
        return result.toString();

    }


    public String convert2(String s, int r) {
        if (r < 2 || s.length() == 0) {
            return s;
        }
        int key = -1; //递增默认1  因为从左到右  从上到下


        List<StringBuilder> rows = new ArrayList<>();
        for (int i = 0; i < r; i++) {
            rows.add(new StringBuilder());
        }
        int i = 0;//从第一个开始遍历
        for (char c : s.toCharArray()) {
            rows.get(i).append(c);
            if (i == 0 || i == r - 1) {
                key = -key;
            }
            i += key;
        }
        StringBuilder result = new StringBuilder();
        for (StringBuilder b :
                rows) {
            result.append(b);
        }
        return result.toString();

    }
}
