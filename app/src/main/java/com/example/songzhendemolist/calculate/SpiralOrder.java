package com.example.songzhendemolist.calculate;

import android.util.Log;

/**
 * The author：dp
 * Date：2022/8/24 09:24
 * Des: 顺时针打印矩阵  https://leetcode.cn/problems/shun-shi-zhen-da-yin-ju-zhen-lcof/
 */
public class SpiralOrder implements ICalculate {
    @Override
    public void calculate() {
//        int[][] matrix = new int[][]{{1,2,3},{4,5,6},{7,8,9}};
//        int[] order = spiralOrder2(matrix);
        test1();
    }

    public void test1() {
        int a = 0;
        int b = 0;
        int i = 0;
        int j = 0;
        a = ++i;
        b= j++;

        /**
         * i++  和 ++i 单独使用的时候是一样的 相当于 i= i+1
         * 如果 a = i++ 相当于 a = i  i = i+1  先赋值再运算
         * 如果 a = ++i  相当于 i= i+1 a = i   先运算再赋值
         *
         * count=count++ 这里的count++是一个表达式，是有返回值的，
         * 它的返回值就是count自增前的值，java对于自增是这样处理的：
         * 先把count的值（注意是值，不是引用）拷贝到一个临时变量区，然后对count变量加1，最后返回临时变量区的值。
         * 程序第一次循环时的详细处理步骤如下：
         * 步骤1：JVM把count的值拷贝到临时变量区
         * 步骤2：count值加1，这时候count的值是1
         * 步骤3：返回临时变量区的值，0
         * 步骤4：返回值赋值给count，此时count值被重置成0.
         */
        Log.d("sz--","b   "+b+"   a  "+a);

    }

    /**
     * 按模拟层
     *
     * @param matrix
     * @return
     */
    public int[] spiralOrder(int[][] matrix) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return new int[0];
        }
        int rows = matrix.length, cols = matrix[0].length;
        int[] ans = new int[rows * cols];
        int left = 0, right = cols - 1, top = 0, bottom = rows - 1;
        int index = 0;
        while (left <= right && top <= bottom) {
            for (int col = left; col <= right; col++) {
                ans[index++] = matrix[top][col];
            }

            for (int row = top + 1; row <= bottom; row++) {
                ans[index++] = matrix[row][right];
            }

            if (left < right && top < bottom) {
                for (int col = right - 1; col > left; col--) {
                    ans[index++] = matrix[bottom][col];
                }

                for (int row = bottom; row > top; row--) {
                    ans[index++] = matrix[row][left];
                }
            }

            left++;
            right--;
            top++;
            bottom--;
        }

        return ans;
    }
}
