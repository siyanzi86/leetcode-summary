package com.example.songzhendemolist.calculate;

/**
 * The author：dp
 * Date：2022/12/29 10:05
 * Des:https://leetcode.cn/problems/search-insert-position/description/
 */
public class SearchInsert implements ICalculate {
    @Override
    public void calculate() {

    }

    /**
     *
     * 需要满足：nums[pos−1]<target≤nums[pos]  为什么不能大于等于   假如插入的值比数组中的元素都小  大于等于肯定不满足了
     * @param nums
     * @param target
     * @return
     */
    public int searchInsert(int[] nums, int target) {
        int len = nums.length;
        int start = 0, end = len - 1;
        int index = len;

        while (start <= end) {
            int mid = start + (end - start) / 2;

            if (nums[mid] < target){
                start = mid+1;

            }else {
                index = mid;
                end = mid-1;
            }
        }

        return index;

    }
}
