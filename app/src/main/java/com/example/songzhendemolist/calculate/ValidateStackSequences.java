package com.example.songzhendemolist.calculate;

import java.util.Stack;

/**
 * The author：dp
 * Date：2022/8/26 09:55
 * Des:输入两个整数序列，第一个序列表示栈的压入顺序，请判断第二个序列是否为该栈的弹出顺序。
 * 假设压入栈的所有数字均不相等。例如，序列 {1,2,3,4,5} 是某栈的压栈序列，序列 {4,5,3,2,1}
 * 是该压栈序列对应的一个弹出序列，但 {4,3,5,1,2} 就不可能是该压栈序列的弹出序列。
 */
public class ValidateStackSequences implements ICalculate {
    @Override
    public void calculate() {

    }

    public boolean validateStackSequences(int[] pushed, int[] popped) {
        //解法1：
//        Stack<Integer> stack = new Stack<>();
//        for (int i = 0, j = 0; i < pushed.length; i++) {
//            stack.push(pushed[i]);
//
//            while (stack.size() > 0 && stack.peek() == popped[j]) {
//                stack.pop();
//                j++;
//            }
//        }
//        return stack.size() == 0;
        //解法2：
        int i = 0, j = 0;
        for (int e : pushed) {
            pushed[i] = e;
            while (i >= 0 && pushed[i] == popped[j]) {
                j++;
                i--;
            }
            i++;
        }
        return i == 0;
    }

}
