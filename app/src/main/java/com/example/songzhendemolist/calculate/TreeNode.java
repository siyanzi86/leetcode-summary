package com.example.songzhendemolist.calculate;

/**
 * The author：dp
 * Date：2022/8/29 09:57
 * Des:
 */
public class TreeNode {

    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
}
