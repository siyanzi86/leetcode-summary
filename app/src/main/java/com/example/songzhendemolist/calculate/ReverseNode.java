package com.example.songzhendemolist.calculate;

import android.util.Log;

/**
 * The author：dp
 * Date：2022/8/22 09:14
 * Des:反转链表  定义一个函数，输入一个链表的头结点，反转该链表并输出反转后链表的头结点。
 * https://leetcode.cn/problems/UHnkqh/solutions/
 */
public class ReverseNode implements ICalculate {

    @Override
    public void calculate() {

    }

    /**
     * 1->2->3->null ----->   null->3->2->1
     *
     * @param head
     * @return
     */

    private ListNode reverseNode(ListNode head) {
        ListNode curr = head;
        ListNode pre = null;
        while (curr != null) {
            //先记录下次的头结点
            ListNode next = curr.next;
            //将当前节点的下一个节点指向前一个节点 第一次前一个节点是null 再次赋值为当前节点
            curr.next = pre;
            pre = curr;
            curr = next;
        }
        return pre;
    }

    /**
     * 递归方式遍历
     *
     * @param root 头结点
     */
    private void print(ListNode root) {
        if (root != null) {
            print(root.next);
            Log.d("sz", root.val + "----");
        }
    }
}
