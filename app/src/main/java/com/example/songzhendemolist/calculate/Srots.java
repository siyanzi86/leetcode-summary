package com.example.songzhendemolist.calculate;

import android.util.Log;

public class Srots implements ICalculate {
    @Override
    public void calculate() {
        int[] nums = {4, 7, 1, 3, 6, 7, 9};
//        int[] arr = q(nums, 0, nums.length - 1);
        insertionSort(nums);
        for (int i = 0; i < nums.length; i++) {
            Log.d("sz--", nums[i] + "--");
        }
    }

    private int[] q(int[] arr, int left, int right) {
        if (left < right) {
            int indext = index(arr, left, right);
            q(arr, left, indext - 1);
            q(arr, indext + 1, right);
        }
        return arr;

    }

    private int index(int[] arr, int left, int right) {
        int start = left;
        int index = start + 1;
        for (int i = index; i <= right; i++) {
            if (arr[i] < arr[start]) {
                swap(arr, i, index);
                index++;
            }
        }
        swap(arr, start, index - 1);
        return index - 1;
    }


    private int[] quickSort(int[] arr, int left, int right) {
        if (left < right) {
            int partitionIndex = partition(arr, left, right);
            quickSort(arr, left, partitionIndex - 1);
            quickSort(arr, partitionIndex + 1, right);
        }
        return arr;
    }

    private int partition(int[] arr, int left, int right) {
        // 设定基准值（pivot）
        int pivot = left;
        int index = pivot + 1;
        for (int i = index; i <= right; i++) {
            if (arr[i] < arr[pivot]) {
                swap(arr, i, index);
                index++;
            }
        }
        swap(arr, pivot, index - 1);
        return index - 1;
    }

    private void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    private void insert(int[] nums) {
        for (int i = 1; i < nums.length; i++) {
            int temp = nums[i];
            int j = i;

            while (j > 0 && temp < nums[j - 1]) {
                nums[j] = nums[j - 1];
                j--;
            }

            if (i != j) {
                nums[j] = temp;
            }
        }
    }


    private void insertionSort(int[] nums) {
        for (int i = 1; i < nums.length; i++) {
            int j = i;
            int temp = nums[i];
            while (j > 0 && temp < nums[j - 1]) {
                nums[j] = nums[j - 1];
                j--;
            }
            if (j != i) {
                nums[j] = temp;
            }
        }
    }

    private void bubbleSort(int[] nums) {
        for (int i = 0; i < nums.length - 1; i++) {
            boolean isSort = true;
            for (int j = 0; j < nums.length - i - 1; j++) {
                if (nums[j] > nums[j + 1]) {
                    int temp = nums[j];
                    nums[j] = nums[j + 1];
                    nums[j + 1] = temp;
                    isSort = false;
                }
            }

            if (isSort) {
                break;
            }
        }
    }


    private void selectionSort(int[] nums) {
        for (int i = 0; i < nums.length - 1; i++) {

            int min = i;
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[min] > nums[j]) {
                    min = j;
                }
            }
            if (i != min) {
                int temp = nums[min];
                nums[min] = nums[i];
                nums[i] = temp;
            }
        }
    }
}
