package com.example.songzhendemolist.calculate;

/**
 * The author：dp
 * Date：2022/8/23 10:12
 * Des:
 */
public class ListNode {

    int val;
    ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}
