package com.example.songzhendemolist.calculate;

/**
 * The author：dp
 * Date：2022/3/29 10:36
 * Des: 判断回文数
 */
public class IsPalindrome implements ICalculate {
    @Override
    public void calculate() {

    }

    /**
     * 负数不是回文数 x<0
     * 最后一位是0
     * int res = 0;
     * int last = 0;
     * while (x != 0) {
     * //每次取末尾数字
     * int tmp = x % 10;
     * last = res;
     * res = res * 10 + tmp;
     * //判断整数溢出
     * if (last != res / 10) {
     * return false;
     * }
     * x /= 10;
     * }
     *
     * @param x
     * @return
     */
    public boolean isPalindrome(int x) {
        if (x < 0 || x % 10 == 0 && x != 0) {
            return false;
        }
        int reverseNum = 0;
        while (x > reverseNum) {
            reverseNum = reverseNum * 10 + x % 10;
            x = x / 10;
        }
        return reverseNum / 10 == x || reverseNum == x;


    }

    public boolean isPalindrome2(int x) {
        if (x < 0 || x % 10 == 0 && x != 0) {
            return false;
        }
        return isPalindromic(String.valueOf(x));
    }

    public boolean isPalindromic(String s) {
        for (int i = 0; i < s.length() / 2; i++) {
            if (s.charAt(i) != s.charAt(s.length() - 1 - i)) {
                return false;
            }
        }
        return true;
    }
}
