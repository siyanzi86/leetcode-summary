package com.example.songzhendemolist.calculate;

import android.util.Log;

/**
 * The author：dp
 * Date：2022/8/5 10:30
 * Des:请实现一个函数，把字符串中的每个空格替换成"%20"，例如“We are happy.”，则 输出“We%20are%20happy.”。
 * <p>
 * 先判断字符串中空格的数量。根据数量判断该字符串有没有足够的空间替换 成"%20"。
 * 如果有足够空间，计算出需要的空间。根据最终需要的总空间，维护一个指针在最 后。从后到前，遇到非空的就把该值挪到指针指向的位置，
 * 然后指针向前一位，遇 到“ ”，则指针前移，依次替换为“02%”。
 */
public class ReplaceSpace implements ICalculate {
    @Override
    public void calculate() {
        String oldStr = "We are happy%20";
        Log.d("sz--", oldStr.length() + " =======");
    }

    public String replaceSpace(String s) {

        int len = s.length();
        int size = 0;
        char[] array = new char[len * 3];

        for (int i = 0; i < len; i++) {
            char ch = s.charAt(i);

            if (ch == ' ') {
                array[size++] = '%';
                array[size++] = '2';
                array[size++] = '0';
            } else {
                array[size++] = ch;
            }
        }

        return new String(array, 0, size);
    }

}
