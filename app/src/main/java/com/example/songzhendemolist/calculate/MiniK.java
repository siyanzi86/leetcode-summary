package com.example.songzhendemolist.calculate;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * The author：dp
 * Date：2022/9/2 09:56
 * Des:最小的前k个数
 */
public class MiniK implements ICalculate {
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void calculate() {

        getLeastNumbers(new int[]{9, 3, 7, 5, 10, 2, 4, 5}, 4);

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public int[] getLeastNumbers(int[] arr, int k) {

        if (arr == null || k < 1 || k > arr.length) {
            return new int[0];
        }
        int[] vec = new int[k];

        PriorityQueue<Integer> queue = new PriorityQueue<Integer>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        });

        for (int i : arr) {
            if (queue.size() < k) {
                queue.offer(i);
            } else {
                if (queue.peek() > i) {
                    queue.poll();
                    queue.offer(i);
                }
            }
        }
        for (int i = 0; i < k; i++) {
            vec[i] = queue.poll();
        }

        return vec;
    }
}
