package com.example.songzhendemolist.calculate;

/**
 * 最长公共字串
 */
public class LongestCommonPrefix implements ICalculate {

    @Override
    public void calculate() {

    }

    public String longestCommonPrefix(String[] strs) {

        if (strs == null || strs.length == 0) {
            return "";
        }

        String prefix = strs[0];
        for (int i = 1; i < strs.length; i++) {
            prefix = getPrefix(prefix, strs[i]);
            if (prefix.length() == 0) {
                break;
            }
        }
        return prefix;

    }

    private String getPrefix(String s1, String s2) {
        int min = Math.min(s1.length(), s2.length());
        int index = 0;

        while (index < min && s1.charAt(index) == s2.charAt(index)) {
            index++;
        }
        return s1.substring(0, index);
    }
}
