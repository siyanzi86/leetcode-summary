package com.example.songzhendemolist.calculate;

import android.util.Log;
import android.util.SparseArray;

import java.util.Arrays;

public class BinarySearch implements ICalculate {
    @Override
    public void calculate() {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        Arrays.sort(arr);
        Log.d("sz--", "result:  " + searchTarget(arr, 6));

    }

    public int searchTarget(int[] arr, int target) {
        int start = 0;
        int end = arr.length - 1;
        while (start <= end) {
            int mid = (end + start) >>> 1;
            if (target > arr[mid]) {
                start = mid + 1;
            } else if (target < arr[mid]) {
                end = mid - 1;
            } else {
                return mid;
            }
        }
        return ~start;
    }

}
