package com.example.songzhendemolist.calculate;

import android.util.Log;

/**
 * The author：dp
 * Date：2022/3/11 10:02
 * Des:
 * https://leetcode.cn/problems/reverse-integer/
 * 考虑益处问题：两种方式 1.INT_MAXVALUE/10 2.溢出之后/10 与原值不一样
 */
public class Reverse implements ICalculate {
    @Override
    public void calculate() {
        Log.d("sz--", reverse(-456789) + "----");
    }

    public int reverse2(int x) {
        int ans = 0;
        while (x != 0) {
            if (ans < Integer.MIN_VALUE / 10 || ans > Integer.MAX_VALUE / 10) {
                return 0;
            }
            int carry = x % 10;
            ans = ans * 10 + carry;
            x = x / 10;
        }
        return ans;
    }

    public int reverse(int x) {
        int last = 0;
        int ans = 0;

        while (x != 0) {
            last = ans;
            ans = ans * 10 + x % 10;
            if (last != ans / 10) {
                return 0;
            }
            x /= 10;
        }

        return ans;
    }
}
