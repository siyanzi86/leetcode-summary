package com.example.songzhendemolist.calculate;

import android.util.Log;

/**
 * The author：dp
 * Date：2022/3/4 10:44
 * Des:
 * 给你一个字符串 s，找到 s 中最长的回文子串。
 * <p>
 *  
 * <p>
 * 示例 1：
 * <p>
 * 输入：s = "babad"
 * 输出："bab"
 * 解释："aba" 同样是符合题意的答案。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/longest-palindromic-substring
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class LongestPalindrome implements ICalculate {
    @Override
    public void calculate() {
        Log.d("sz--", 2 % 4 + "-------------->" + longestPalindrome("cddcba"));

        for (int i = 0; i < 4; i++) {
            Log.d("sz--", "---" + i + (i % 4 < 2 ? "true" : "f"));
        }
    }

    /**
     * 中心扩展  枚举会回文中心 对应1或2的边界
     */

    private String getLongest(String s) {
        if (s == null || s.length() < 1) {
            return "";
        }
        int start = 0, end = 0;

        for (int i = 0; i < s.length(); i++) {
            int len1 = expandAroundCenter(s, i, i);//边界是1个情况 如aba
            int len2 = expandAroundCenter(s, i, i + 1);//边界是两个的情况abba
            int len = Math.max(len1, len2);

            if (len > end - start) {
                start = i - (len - 1) / 2;//i往前挪回文数长度的一半
                end = i + len / 2;
            }
        }

        return s.substring(start, end + 1);

    }


    public int expandAroundCenter(String s, int left, int right) {
        while (left >= 0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
            --left;
            ++right;
        }
        return right - left - 1;
    }


    /**
     * 暴力破解
     *
     * @param s
     * @return
     */
    public String longestPalindrome(String s) {
        String ans = "";
        int max = 0;
        int len = s.length();
        for (int i = 0; i < len; i++)
            for (int j = i + 1; j <= len; j++) {
                String test = s.substring(i, j);
                if (isPalindromic(test) && test.length() > max) {
                    ans = s.substring(i, j);
                    max = Math.max(max, ans.length());
                }
            }
        return ans;
    }

    public boolean isPalindromic(String s) {
        int length = s.length();
        for (int i = 0; i < length / 2; i++) {
            if (s.charAt(i) != s.charAt(length - i - 1)) {
                return false;
            }
        }
        return true;
    }

}
