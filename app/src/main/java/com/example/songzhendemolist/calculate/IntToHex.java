package com.example.songzhendemolist.calculate;

import android.util.Log;

/**
 * The author：dp
 * Date：2023/3/27 11:39
 * Des:
 */
public class IntToHex implements ICalculate {
    @Override
    public void calculate() {
        Log.d("sz--","-------"+intToHex(22));
        Log.d("sz--","========="+Integer.toHexString(22));
    }


    private String intToHex(int n) {
        StringBuilder stringBuilder = new StringBuilder();

        char[] hex_char = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

        while (n != 0) {
            stringBuilder.append(hex_char[n % 16]);
            n /= 16;
        }

        return stringBuilder.reverse().toString();
    }
}
