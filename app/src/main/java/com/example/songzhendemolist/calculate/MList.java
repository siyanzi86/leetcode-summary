package com.example.songzhendemolist.calculate;

import android.util.Log;

import java.util.Stack;

/**
 * 用两个栈实现一个队列。队列的声明如下，请实现它的两个函数appendTail 和
 * deleteHead，分别完成在队列尾部插入结点和在队列头部删除结点的功能。
 * 二、解题思路
 * 栈1用于存储元素，栈2用于弹出元素，负负得正。
 * 说的通俗一点，现在把数据1、2、3分别入栈一，然后从栈一中出来（3、2、1），
 * 放到栈二中，那么，从栈二中出来的数据（1、2、3）就符合队列的规律了，即负
 * 负得正。
 * <p>
 * 队列是先进先出的规则  所以只要实现规则这个就可以
 */
public class MList implements ICalculate {
    @Override
    public void calculate() {
        Queue<String> names = new Queue<>();
        names.appendTail("张三");
        names.appendTail("李四");
        names.appendTail("王五");

        String head = names.deleteHead();
        Log.d("sz--", "head " + head);
    }


    public static class Queue<T> {

        private final Stack<T> stack1 = new Stack<>();//只插入数据
        private final Stack<T> stack2 = new Stack<>();//只弹出数据

        public void appendTail(T t) {
            stack1.add(t);
        }


        public T deleteHead() {
            if (stack2.isEmpty()) {
                while (!stack1.isEmpty()) {
                    stack2.add(stack1.pop());
                }
            }

            if (stack2.isEmpty()) {
                throw new RuntimeException("no more element.");
            }

            return stack2.pop();
        }
    }


}
