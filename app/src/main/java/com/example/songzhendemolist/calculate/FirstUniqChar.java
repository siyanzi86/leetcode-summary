package com.example.songzhendemolist.calculate;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.HashMap;
import java.util.Map;

/**
 * The author：dp
 * Date：2022/9/8 11:20
 * Des:第一个只出现一次的字符
 */
public class FirstUniqChar implements ICalculate {
    @Override
    public void calculate() {

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public char firstUniqChar(String s) {
        Map<Character, Integer> map = new HashMap<>();

        for (char c : s.toCharArray()) {
           map.put(c,map.getOrDefault(c,0)+1);
        }

        for (int i = 0; i < s.length(); i++) {
            if (map.get(s.charAt(i))==1){
                return s.charAt(i);
            }
        }

        return ' ';
    }

}
