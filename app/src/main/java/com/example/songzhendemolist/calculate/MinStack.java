package com.example.songzhendemolist.calculate;

import android.util.SparseArray;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Set;

/**
 * The author：dp
 * Date：2022/8/25 10:58
 * Des:定义栈的数据结构，请在该类型中实现一个能够得到栈的最小元素的 min 函数在该栈中，调用 min、push 及 pop 的时间复杂度都是 O(1)。
 */
public class MinStack implements ICalculate {

    @Override
    public void calculate() {

    }

    Deque<Integer> stack;
    Deque<Integer> minStack;

    public MinStack() {
        stack = new LinkedList<>();
        minStack = new LinkedList<>();
        minStack.push(Integer.MAX_VALUE);
    }

    public void push(int x) {
        stack.push(x);
        minStack.push(Math.min(x,minStack.peek()));
    }

    public void pop() {

            stack.pop();
            minStack.pop();

    }

    public int top(){
        return stack.peek();
    }

    public int min() {
        return minStack.pop();
    }
}
