package com.example.songzhendemolist.calculate;

import android.util.Log;

import java.util.Stack;

/**
 * The author：dp
 * Date：2022/8/10 10:21
 * Des:输入个链表的头结点，从尾到头反过来打印出每个结点的值。 二、解题思路 使用栈的方式进行。 将链表从头到尾压入栈内，出栈的过程就对应着从尾到头。
 */
public class PrintListInverselyUsingIteration implements ICalculate {

    @Override
    public void calculate() {
        ListNode node1 = new ListNode();
        ListNode node2 = new ListNode();
        ListNode node3 = new ListNode();
        ListNode node4 = new ListNode();
        ListNode node5 = new ListNode();
        ListNode node6 = new ListNode();
        ListNode node7 = new ListNode();
        ListNode node8 = new ListNode();
        node1.val = 1;
        node2.val = 2;
        node3.val = 3;
        node4.val = 4;
        node5.val = 5;
        node6.val = 6;
        node7.val = 7;
        node8.val = 8;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;
        node6.next = node7;
        node7.next = node8;
//        printWithStack(node1);
        ListNode node = deleteNode(node1, node5);
    }



    public static ListNode deleteNode(ListNode head, ListNode toBeDeleted) {
//         如果输入参数有空值就返回表头结点
        if (head == null || toBeDeleted == null) {
            return head;
        }
//         如果删除的是头结点，直接返回头结点的下一个结点
        if (head == toBeDeleted) {
            return head.next;
        }
//         下面的情况链表至少有两个结点
//         在多个节点的情况下，如果删除的是最后一个元素
        if (toBeDeleted.next == null) {
            // 找待删除元素的前驱
            ListNode tmp = head;
            while (tmp.next != toBeDeleted) {
                tmp = tmp.next;
            }
            // 删除待结点
            tmp.next = null;
        }
//         在多个节点的情况下，如果删除的是某个中间结点
        else {
            toBeDeleted.val = toBeDeleted.next.val;
            toBeDeleted.next = toBeDeleted.next.next;

        }
//         返回删除节点后的链表头结点
        return head;
    }


    public ListNode deleteNode(ListNode head, int val) {

        if (head.val == val) {
            return head.next;
        }

        ListNode p = head;

        while (p.next != null && p.next.val != val) {
            p = p.next;
        }

        if (p.next != null) {
            p.next = p.next.next;
        }

        return head;
    }


    /**
     * 放入栈的方式遍历
     *
     * @param root 头结点
     */
    private void printWithStack(ListNode root) {
        Stack<ListNode> stack = new Stack<>();
        while (root != null) {
            stack.push(root);
            root = root.next;
        }
        ListNode temp;
        while (!stack.isEmpty()) {
            temp = stack.pop();
            Log.d("sz", temp.val + "------");
        }
    }

    /**
     * 递归方式遍历
     *
     * @param root 头结点
     */
    private void print(ListNode root) {
        if (root != null) {
            print(root.next);
            Log.d("sz", root.val + "----");
        }

    }


}
