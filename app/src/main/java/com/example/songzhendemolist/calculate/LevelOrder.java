package com.example.songzhendemolist.calculate;

import android.util.Log;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * The author：dp
 * Date：2022/8/29 09:57
 * Des:
 */
public class LevelOrder implements ICalculate {


    public int[] levelOrder(TreeNode root) {
        if (root == null) return new int[0];

        Deque<TreeNode> deque = new LinkedList<>();
        List<Integer> integers = new ArrayList<>();
        deque.add(root);
        TreeNode current;
        while (!deque.isEmpty()) {
            current = deque.remove();
            integers.add(current.val);

            if (current.left != null) {
                deque.add(current.left);
            }

            if (current.right != null) {
                deque.add(current.right);
            }
        }

        int[] res = new int[integers.size()];
        for (int i = 0; i < integers.size(); i++) {
            res[i] = integers.get(i);
        }

        return res;
    }

    @Override
    public void calculate() {

        List<String> list = new ArrayList<>();
        list.add(null);
        list.add(null);
        Log.d("sz--",list.size()+"---"+(list.get(0)==null?"null":"no null"));

    }
}
