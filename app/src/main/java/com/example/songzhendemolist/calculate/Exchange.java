package com.example.songzhendemolist.calculate;

import android.util.Log;

/**
 * The author：dp
 * Date：2022/8/18 10:46
 * Des:输入一个整数数组，实现一个函数来调整该数组中数字的顺序，使得所有奇数在数组的前半部分，所有偶数在数组的后半部分。
 * 作者：力扣官方题解
 * 链接：https://leetcode.cn/problems/diao-zheng-shu-zu-shun-xu-shi-qi-shu-wei-yu-ou-shu-qian-mian-lcof/solutions/1785640/diao-zheng-shu-zu-shun-xu-shi-qi-shu-wei-en35/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class Exchange implements ICalculate {
    @Override
    public void calculate() {
        int[] res = exchange1(new int[]{1, 2, 3, 4, 5, 6});
        Log.d("sz--", "-----");
    }

    /**
     * 两端指针  交换
     *
     * @param arr
     * @return
     */
    public int[] exchange(int[] arr) {
        if (arr == null || arr.length == 1) {
            return arr;
        }

        int start = 0;
        int end = arr.length - 1;

        while (start < end) {
            if ((arr[start] & 1) == 0) {
                if ((arr[end] & 1) == 0) {
                    end--;
                } else {
                    int temp = arr[start];
                    arr[start] = arr[end];
                    arr[end] = temp;
                }
            } else {
                start++;
            }
        }
        return arr;
    }

    /**
     * 两端指针  交换
     *
     * @param nums
     * @return
     */
    public int[] exchange2(int[] nums) {
        int left = 0, right = nums.length - 1;
        while (left < right) {
            while (left < right && nums[left] % 2 == 1) {
                left++;
            }
            while (left < right && nums[right] % 2 == 0) {
                right--;
            }
            if (left < right) {
                int temp = nums[left];
                nums[left] = nums[right];
                nums[right] = temp;
                left++;
                right--;
            }
        }
        return nums;
    }

    /**
     * 双指针  开辟新空间
     *
     * @param nums
     * @return
     */
    public int[] exchange3(int[] nums) {

        int n = nums.length;

        int[] ans = new int[n];

        int left = 0;
        int right = n - 1;

        for (int num : nums) {
            if ((n & 1) == 0) {
                ans[right--] = num;
            } else {
                ans[left++] = num;
            }
        }

        return ans;
    }


    /**
     * 快慢指针  交换
     *
     * @param nums
     * @return
     */

    public int[] exchange1(int[] nums) {
        int fast = 0, slow = 0;

        while (fast < nums.length) {
            if ((nums[fast] & 1) == 1) {
                if (slow != fast) {
                    int temp = nums[fast];
                    nums[fast] = nums[slow];
                    nums[slow] = temp;
                }
                slow++;
            }
            fast++;
        }

        return nums;

    }

}
