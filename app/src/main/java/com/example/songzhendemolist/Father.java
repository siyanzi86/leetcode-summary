package com.example.songzhendemolist;

import android.util.Log;

/**
 * The author：dp
 * Date：2022/3/2 10:12
 * Des:
 */
public abstract class Father {

    public Father() {

    }

    public final static String name = "123";
    private int age = 1;

    protected void doSomeThing() {
        Log.d("sz--", "Father doSomeThing----");
    }

    protected void doSomeThing(String str) {

    }

    public static void doSomeThing(String str, int i) {

    }

    private void a() {
        new InnerClass().c();
        new StaticInnerClass().b();
    }

    protected abstract void getInfo();

    public class InnerClass {
        public String c = "";
        public void c() {
            //非静态内部类可以访问外部类的所有成员


        }
    }

    public static class StaticInnerClass {
        public static String b = "";
        public void b() {

        }

    }


}

