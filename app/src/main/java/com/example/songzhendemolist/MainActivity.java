package com.example.songzhendemolist;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.example.songzhendemolist.calculate.ICalculate;
import com.example.songzhendemolist.calculate.IntToHex;
import com.example.songzhendemolist.calculate.RemoveElement;
import com.example.songzhendemolist.calculate.ReplaceSpace;
import com.example.songzhendemolist.calculate.Reverse;
import com.example.songzhendemolist.calculate.Srots;

import java.util.Arrays;
import java.util.Comparator;
import java.util.concurrent.atomic.AtomicInteger;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.btn).setOnClickListener(v -> {
            ICalculate iCalculate = new IntToHex();
            iCalculate.calculate();
//            Integer[] arr = new Integer[]{1, 1, 2, 2, 5, 5, 5, 6, 7, 8, 9, 9};
//
//            Arrays.sort(arr, new Comparator<Integer>() {
//                @Override
//                public int compare(Integer o1, Integer o2) {
//                    return o2 - o1;
//                }
//            });
//            for (int i = 0; i < arr.length; i++) {
//                Log.d("sz--", arr[i] + "-----------");
//            }

            AtomicInteger atomicInteger = new AtomicInteger(100);

            boolean isSucceed = atomicInteger.compareAndSet(100, 110);

            Log.d("sz--", isSucceed + " -- " + atomicInteger.get());

            boolean isFail = atomicInteger.compareAndSet(100, 120);

            Log.d("sz--", isFail + " == " + atomicInteger.get());

            Log.d("sz--",Integer.MAX_VALUE+"    "+Integer.MIN_VALUE);
        });
    }
}