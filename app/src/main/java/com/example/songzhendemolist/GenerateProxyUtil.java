package com.example.songzhendemolist;

import android.util.Log;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * The author：dp
 * Date：2022/2/22 21:08
 * Des:
 */
public class GenerateProxyUtil {
    /**
     * 动态代理：JDK提供的相关api:
     *                    java.lang.reflect.InvocationHandler和java.lang.reflect.Proxy来实现
     *
     *
     *                    动态代理生成的类都继承自proxy 实现目标类实现的接口  从而可以调用接口方法
     */

    public static Object loadProxy(Object target) throws Exception {
        Class<?> proxyClass = Proxy.getProxyClass(target.getClass().getClassLoader(), target.getClass().getInterfaces());
        Constructor<?> constructors = proxyClass.getConstructor(InvocationHandler.class);
        Object proxy = constructors.newInstance(new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                Log.d("sz--","执行开始-----1-------");

                Object result = method.invoke(target,args);

                Log.d("sz--","执行完毕------1------");

                return result;
            }
        });

        return proxy;
    }

    public static Object loadProxyInstance(Object target){
        return Proxy.newProxyInstance(target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

                        Log.d("sz--","执行开始------2------");

                        Object result = method.invoke(target,args);

                        Log.d("sz--","执行完毕-------2-----");

                        return result;
                    }
                });
    }
}
